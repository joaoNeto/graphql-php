<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert([
            [
                'id' => 1,
                'author_id' => 1,
                'title' => 'my title 1',
                'content' => 'my content 1'
            ],
            [
                'id' => 2,
                'author_id' => 1,
                'title' => 'my title 2',
                'content' => 'my content 2'
            ],
            [
                'id' => 3,
                'author_id' => 1,
                'title' => 'my title 3',
                'content' => 'my content 3'
            ],
            [
                'id' => 4,
                'author_id' => 2,
                'title' => 'my title',
                'content' => ' my content'
            ]
        ]);
    }
}
