<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'id' => 1,
                'name' => 'joão neto',
                'email' => 'joao.neto@madeiramadeira.com.br',
                'password' => 'e8d95a51f3af4a3b134bf6bb680a213a',
                'remember_token' => 'token'
            ],
            [
                'id' => 2,
                'name' => 'djalma',
                'email' => 'dj.alma@madeiramadeira.com.br',
                'password' => 'e8d95a51f3af4a3b134bf6bb680a213a',
                'remember_token' => 'token'
            ],
        ]);
    }
}
